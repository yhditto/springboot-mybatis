springboot整合mybatis并配置log4j2打印sql以及调试信息。
一.mybatis有两种使用方式。一种是注解另外一只是配置mapper.xml
 
 1.mybatis xml方式使用：
@Mapper
在接口上添加@Mapper 注解，声明对应接口是一个Mapper.xml
声明xml里面对应的方法即可，最后service注入mapper。

2.注解方式使用
    在接口上添加@Mapper 注解
     * 传参方式:
     * 1> 使用@Param
     * 2> 使用Map
     * 3> 使用对象
     *
     * 增删改查,MyBatis针对不同的数据库操作分别提供了不同的注解来进行配置:
     * @Insert
     * @Select
     * @Update
     * @Delete
     *
     *  具体详解请查看：http://www.mybatis.org/mybatis-3/zh/java-api.html
     */

  返回结果的绑定 ,通过@Results 和@Result 来绑定
  
     *  通过情况下，没有配置的话，字段不相同的就不会对应上，同时如果配置了结果绑定时，部分字段没有对应的话，那么对应字段的值为null
     * **/


二.log4j2配置：
 SpringBoot默认使用了logback，所以要使用log4j2，要先去除spring boot的默认logging包

<!-- 去除spring-boot默认日志依赖包 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
    <version>${spring.boot.starter.version}</version>
    <exclusions>
        <!-- 想要配置log4j2，就要先去除logging包 -->
        <exclusion>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-logging</artifactId>
        </exclusion>
    </exclusions>
</dependency>

    添加spring-boot-starter-log4j2的依赖包

<!-- 添加spring-boot-starter-log4j2的依赖包 -->
<!-- day04 使用log4j2添加包，此时版本是log4j 2.6.2 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-log4j2</artifactId>
    <version>${spring.boot.starter.version}</version>
</dependency>


    在application.properties中设置log4j2配置文件路径

logging.config=classpath:log4j2.xml

log4j2配置

    Appenders：
        作用：定义日志输出位置（控制台、文件或其他）、内容结构（起始就是PatternLayout）、日志等级（不完全由Appenders确定）。
        不能做：哪些类使用该Appender
    Loggers:
        作用：定义哪些类使用那些Appender，并可以设置日志级别

