package com.yang.service;

import com.yang.mapper.UserMapper;
import com.yang.xmlmapper.XUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yang.domain.User;

import java.util.List;

/**
 * Created by yang on 2017/4/25.
 */
@Service
public interface UserService {

    /**
     * 根据id 获取用户信息
     * @param id
     * @return
     */
    /*
    以注解的方式
     */
    public User getUserById(int id);
    /*
    以xml方式
     */
    public List<User> getXUserById(int id);

    /**
     * 添加用户
     * @param user
     * @return
     */
    public int add(User user);

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    public int update(User user);


    /**
     * 删除用户信息
     * @param id
     * @return
     */
    public int delete(Long id);
}
