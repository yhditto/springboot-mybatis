package com.yang.service.usersviceimpl;

import com.yang.domain.User;
import com.yang.mapper.UserMapper;
import com.yang.service.UserService;
import com.yang.xmlmapper.XUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yang on 2017/4/25.
 */
@Service
public class UserServiceimpl implements UserService {


    @Autowired
    private UserMapper userMapper;

    @Autowired
    private XUserMapper xUserMapper;

    /**
     * 根据id 获取用户信息
     * @param id
     * @return
     */
    public User getUserById(int id){
        return  userMapper.findById(id);
    }

    public List<User> getXUserById(int id) {return  xUserMapper.findById(id);};

    /**
     * 添加用户
     * @param user
     * @return
     */
    public int add(User user) {
        return userMapper.insert(user.getId(),user.getName(),user.getAge());
    }

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    public int update(User user) {
        return userMapper.update(user);
    }

    /**
     * 删除用户信息
     * @param id
     * @return
     */
    public int delete(Long id) {
        return userMapper.delete(id);
    }
}
