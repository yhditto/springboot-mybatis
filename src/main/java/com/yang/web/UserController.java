package com.yang.web;

import com.yang.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.yang.domain.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by yang on 2017/4/25.
 */
@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    private UserService userService;

    @RequestMapping("/getUserById")
    @ResponseBody
    //http://localhost:8080/getUserById?id=1
    public User getUserById(int id){
         logger.info(userService.getXUserById(id).toString());
         return  userService.getUserById(id);
    }
    @RequestMapping("/getid")
    @ResponseBody//返回list集合
    public List<User> getXmlUserId(int id){
        return userService.getXUserById(id);

    }
    @RequestMapping("/add")
    @ResponseBody
    public int add(){
        User user =new User();
        user.setId( 223);
        user.setName("王尼玛");
        user.setAge(28);
        return  userService.add(user);
    }

    @RequestMapping("/update")
    @ResponseBody
    public int update(){
        User user =new User();
        user.setId(17);
        user.setAge(88);
        user.setName("张三丰");
        return  userService.update(user);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public int delete(Long id){
        return  userService.delete(id);
    }

}
